///////////////// -*- C++ -*- ///////////////////
// Author: A.Sciandra<andrea.sciandra@cern.ch> //
/////////////////////////////////////////////////

// includes
// general ROOT
#include "TCanvas.h"
#include "TH1F.h"
#include "THStack.h"
#include "TF1.h"
#include "TGaxis.h"
#include "TLegend.h"
#include "TFile.h"
#include "TRandom3.h"
#include "TStyle.h"

// RooFit
#include "RooAbsData.h"
#include "RooAbsPdf.h"
#include "RooAddPdf.h"
#include "RooArgList.h"
#include "RooBinSamplingPdf.h"
#include "RooCategory.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooGaussian.h"
#include "RooGenericPdf.h"
#include "RooHist.h"
#include "RooPlot.h"
#include "RooRandom.h"
#include "RooRealVar.h"
#include "RooSimultaneous.h"
#include "RooWorkspace.h"

using namespace std;
using namespace RooFit;

// declare functions defined below

int main(int argc, char**argv) {

  if (argc < 6) {
    cout << "Correct usage: ./exe N_ev N_bins sig_frac integral_precision pois_sample iter[optional]. Exiting..." << endl;
    exit(0);
  }

  // n. of events
  long N_ev = strtol(argv[1], NULL, 10);
  // n. of bins 
  long N_bins = strtol(argv[2], NULL, 10);
  // use RooBinSamplingPdf?
  double sig_frac = stod(argv[3]);
  // use RooBinSamplingPdf?
  double intBins = stod(argv[4]);
  // perform Poissonian sampling?
  bool sample = stoi(argv[5]);
  // iteration number
  long N_iter(-999);
  if (argc>=7) N_iter = strtol(argv[6], NULL, 10);

  // TFile to store RooFitResult
  TString f_name("RooFitResult_N"+(TString)to_string(N_ev)+"_B"+(TString)to_string(N_bins)+"_F"+(TString)to_string(sig_frac)+"_P"+(TString)to_string(intBins)+".root");
  if (argc>=7) f_name.ReplaceAll(".root","_I"+(TString)to_string(N_iter)+".root");
  TFile *f_out = new TFile(f_name,"RECREATE");
  // Declare observable x
  Float_t xmin(70), xmax(230);
  RooRealVar x("x", "M [GeV]", xmin, xmax);
  x.setBins(N_bins);

  // build QCD model
  RooRealVar theta1("theta1", "theta1", -0.7, -100, 100);
  RooRealVar theta2("theta2", "theta2", -0.05, -100, 100);
  RooRealVar theta3("theta3", "theta3", -0.1, -100, 100);
  RooRealVar theta4("theta4", "theta4", 0.05, -100, 100);
  TString qcd_expr("TMath::Exp(@1*TMath::Power((@0-150.)/80.,1)+@2*TMath::Power((@0-150.)/80.,2)+@3*TMath::Power((@0-150.)/80.,3)+@4*TMath::Power((@0-150.)/80.,4))");
  RooGenericPdf bkg("bkg", "Background component", qcd_expr, RooArgList(x, theta1, theta2, theta3, theta4));

  // build Z model
  RooRealVar mean("mean", "mean of gaussians", 91.2, 91.2, 91.2);
  RooRealVar sigma("sigma", "width of gaussians", 8.5, 8.5, 8.5);

  RooGaussian sig("sig", "Signal component", x, mean, sigma);
  //RooGenericPdf sig("sig", "Signal component", "TMath::Gaus(@0,@1,@2)", RooArgList(x, mean, sigma));

  // combine QCD+Z into a RooSimultaneous
  // and generate workspace
  RooRealVar sigfrac("sigfrac", "Fraction of signal component", sig_frac, 0, 1);
  RooAddPdf model("model", "model", RooArgList(sig, bkg), sigfrac);

  // Manually wrap model PDF
  RooBinSamplingPdf binSampler("binSampler", "binSampler", x, model , intBins);
  // Generate a data sample of N_ev events in x from model
  // w/ random seed
  TH1F *h_asimov	= new TH1F("h_asimov", "", N_bins, xmin, xmax); 
  TH1F *h_sampled	= new TH1F("h_sampled", "h_sampled", N_bins, xmin, xmax);
  // THStack
  THStack *hs = new THStack("hs","");
  TH1F *h_QCD		= new TH1F("h_QCD", "", N_bins, xmin, xmax); 
  TH1F *h_Z		= new TH1F("h_Z", "", N_bins, xmin, xmax); 
  // QCD
  TString f_qcd_expr="[0]*"+qcd_expr.ReplaceAll("@0","x").ReplaceAll("@1","[1]").ReplaceAll("@2","[2]").ReplaceAll("@3","[3]").ReplaceAll("@4","[4]");
  cout << f_qcd_expr << endl;
  TF1 *f_qcd = new TF1("f_qcd", f_qcd_expr, xmin, xmax);
  f_qcd->FixParameter(0,1);
  f_qcd->FixParameter(1,theta1.getVal());
  f_qcd->FixParameter(2,theta2.getVal());
  f_qcd->FixParameter(3,theta3.getVal());
  f_qcd->FixParameter(4,theta4.getVal());
  Float_t norm_par = (1-sig_frac)/f_qcd->Integral(xmin,xmax);
  f_qcd->FixParameter(0,norm_par);
  // Z
  TF1* f_Z = new TF1("f_Z", "[0]*TMath::Gaus(x,91.2,8.5)", xmin, xmax);
  f_Z->FixParameter(0,1);
  norm_par = sig_frac/f_Z->Integral(xmin,xmax);
  f_Z->FixParameter(0,norm_par);
  // asimov+sampled datasets
  // If sample==true -> Poisson sampling of data
  TRandom3 trd(0);
  for (int i=1; i<=h_asimov->GetNbinsX(); i++) {
    Float_t x1 = h_asimov->GetBinLowEdge(i);
    Float_t x2 = x1 + h_asimov->GetBinWidth(i);
    // QCD+Z
    Float_t eventsGenerated = N_ev*(f_qcd->Integral(x1,x2) + f_Z->Integral(x1,x2));
    h_asimov->SetBinContent(i,eventsGenerated);
    h_asimov->SetBinError(i,sqrt(eventsGenerated));
    // sampled
    Float_t sampled_eventsGenerated = trd.Poisson(eventsGenerated);
    h_sampled->SetBinContent(i,sampled_eventsGenerated);
    h_sampled->SetBinError(i,sqrt(sampled_eventsGenerated));
    // stack plot
    h_QCD->SetBinContent(i,N_ev*f_qcd->Integral(x1,x2));
    h_Z->SetBinContent(i,N_ev*f_Z->Integral(x1,x2));
  }
  cout << "asimov int:\t" << h_asimov->Integral() << endl;
  cout << "sampled int:\t" << h_sampled->Integral() << endl;

  // RooFit methods unreliable...  
//  RooRandom::randomGenerator()->SetSeed(0);
//  RooAbsData *data = intBins ?	(RooAbsData*)binSampler.generate(x, N_ev, ExpectedData(sample ? kFALSE : kTRUE))
//				: (RooAbsData*)model.generate(x, N_ev, ExpectedData(sample ? kFALSE : kTRUE));

  

  RooDataHist *data = new RooDataHist("data","data",x,Import(sample ? *h_sampled : *h_asimov));
  // Fit RooBinSamplingPdf-wrapped model to data
  RooFitResult* r;
  r = intBins ? binSampler.fitTo(*data, Save()) : model.fitTo(*data, Save());

  // Plot data and PDF overlaid
  TCanvas *c = new TCanvas("c");
  RooPlot *xframe = x.frame(Title("sig+bkg fit"));
  data->plotOn(xframe, DataError(RooAbsData::Poisson));
  if(intBins)	binSampler.plotOn(xframe);
  else		model.plotOn(xframe);
  xframe->Draw();
  c->SaveAs("plot.pdf");

  // Construct a histogram with the pulls of the data w.r.t the curve
  RooHist *hpull = xframe->pullHist();
  // Create a new frame to draw the pull distribution and add the distribution to the frame
  TCanvas *c2 = new TCanvas("c2");
  RooPlot *xframe2 = x.frame(Title("Pull Distribution"));
  xframe2->addPlotable(hpull, "P");
  xframe2->Draw();
  c2->SaveAs("pull.pdf");

  TCanvas *c3 = new TCanvas("c3","c3",800,700);
  TPad *pad = new TPad("p","p",0,0,1,1);
  pad->cd(1);
  gStyle->SetOptStat(0);
  gStyle->SetErrorX(0.);
  TGaxis::SetMaxDigits(3);
  h_asimov->SetLineColor(kBlack);
  h_asimov->SetMarkerStyle(20);
  h_asimov->SetMarkerSize(0.4);
  h_asimov->GetXaxis()->SetTitle("M [GeV]");
  h_asimov->GetXaxis()->SetTitleSize(0.05);
  h_asimov->GetXaxis()->SetLabelSize(0.05);
  h_asimov->GetXaxis()->SetTitleOffset(0.96);
  h_asimov->GetYaxis()->SetTitle(Form("Events / ( %1.0f GeV )", h_asimov->GetBinWidth(1)));
  h_asimov->GetYaxis()->SetTitleOffset(0.9);
  h_asimov->GetYaxis()->SetTitleSize(0.05);
  h_asimov->GetYaxis()->SetLabelSize(0.05);
  h_asimov->GetXaxis()->SetNdivisions(504);;
  h_asimov->GetYaxis()->SetNdivisions(504);;
  h_asimov->Draw("pe1");
  h_QCD->SetFillColorAlpha(kCyan-9,0.4);
  h_Z->SetFillColorAlpha(kOrange+7,0.8);
  h_QCD->SetLineColor(kBlack);
  h_Z->  SetLineColor(kBlack);
  h_QCD->SetLineWidth(0);
  h_Z->  SetLineWidth(0);
  hs->Add(h_QCD); hs->Add(h_Z);
  hs->Draw("same");
  h_asimov->Draw("pe1same");
  TLegend *tl = new TLegend(0.5,0.7,0.8,0.9);
  tl->AddEntry(h_asimov,"Expected data","pe");
  tl->AddEntry(h_Z,"Signal","f");
  tl->AddEntry(h_QCD,"Background","f");
  tl->SetBorderSize(0);
  tl->SetTextSize(0.05);
  tl->SetFillStyle(0);
  tl->Draw("same");
  //TH1F* h_postFit = (TH1F*)(intBins ? binSampler.createHistogram("h_postFit",x) : model.createHistogram("h_postFit",x));
  //h_postFit->Scale(N_ev/h_postFit->Integral());
  //h_postFit->SetLineColor(kRed);
  //h_postFit->Draw("histsame");
  pad->RedrawAxis();
  c3->RedrawAxis();
  c3->SaveAs("ATLAS_plot.pdf");

  r->Write("fitResult") ;
  f_out->Write();
  f_out->Close();
  
  return 0;
}
