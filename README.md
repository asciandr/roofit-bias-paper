[Public project](https://gitlab.cern.ch/asciandr/roobinsamplingpdf-example/-/blob/master/README.md)

Commands:
```
./run_toy_fit.exe [N_ev] [N_bins] [sig_frac] [integral_precision] [pois_sample] [iter_num](optional)
./run_toy_fit.exe 5000 32 0.015 0 1
./run_toy_fit.exe 5000 32 0.015 1e-3 1
```
