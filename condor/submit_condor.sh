#!/bin/bash

# Create required output directories if they don't exist
for dir in out err log output; do
    if ! test -d $dir; then
        mkdir $dir
    fi
done

# Edit files and submit to HTCondor
workdir=${PWD}
mycommand="./run_toy_fit.exe N_ev N_bins sig_frac integral_precision pois_sample iter_num"
echo ${mycommand}
workdir=${workdir%/*}
dir=${workdir##*/}

# set job options
nev="500 5000 50000 50000000"
#nev="50000"
#nbins="8 16 32 128 256 512"
nbins="32"
sigfrac="0.015"
#sigfrac="0.02"
int_prec="0 0.001"
#int_prec="0"
pois_sample="1"
niter=1000
#niter=20
# set job options

echo "Starting loop..."

for Int_prec in ${int_prec}; do
    for N_ev in ${nev}; do
        echo ${N_ev}
        for N_bins in ${nbins}; do
            echo ${N_bins}
            for Sig_frac in ${sigfrac}; do
                echo ${Sig_frac}
#                for (( N=101; N<=$niter; N++ )); do
                for (( N=1; N<=$niter; N++ )); do
                    echo ${N}
                    random=${RANDOM}
                    mycommand_i=$(echo ${mycommand} | sed "s|N_ev|${N_ev}|g" |  sed "s|N_bins|${N_bins}|g" | sed "s|sig_frac|${Sig_frac}|g" | sed "s|integral_precision|${Int_prec}|g" | sed "s|pois_sample|${pois_sample}|g" | sed "s|iter_num|${N}|g")
                    cat job.sh | sed "s|XWORKDIR|${workdir}|g" | sed "s|XCOMMAND|${mycommand_i}|g" > job_${random}.sh
                    chmod 755 job_${random}.sh
                    cat hello.sub | sed "s/XJOBSH/job_${random}.sh/g" > hello_${random}.sub
                    echo hello_${random}.sub
                    echo "running command: ${mycommand_i}"
                    condor_submit hello_${random}.sub
                done
            done
        done
    done
done

