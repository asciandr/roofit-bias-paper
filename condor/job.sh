#!/bin/bash

pwd
localdir="XWORKDIR"

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

# Copy everything necessary from the local directory
cp -rf ${localdir}/Make.sh .
cp -rf ${localdir}/setup.sh .
cp -rf ${localdir}/run_toy_fit.cxx .

# Source setup script
source setup.sh
source Make.sh

# Start setting up command
XCOMMAND

# Copy outputs to local dir
ls -haltr
cp -f RooFitResult_N*.root ${localdir}/condor/output/

echo 'done.'
