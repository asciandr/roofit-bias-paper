#!/usr/bin/env python

import os.path

import ROOT

# invariant names
input_folder	= 'condor/output/'
root		= 'RooFitResult_NXNBEV_BXNBINS_FXSFRAC_PXINTP_IXITN.root'
out_root	= 'out_plots.root'

# variable settings
nev={500,5000,50000,500000,50000000}
#nbins={8,16,32,128,256,512}
nbins={32}
sigfrac={"0.015000"}
int_prec={"0.000000","0.001000"}
niter=1001

f_out		= ROOT.TFile(out_root,'RECREATE')
for nprec,prec in enumerate(int_prec):
#  print(prec)
  for N_ev in nev:
#    print(N_ev)
    for N_bins in nbins:
#      print(N_bins)
      for Sig_frac in sigfrac:
#        print(Sig_frac)
        # histograms
        h_sigfrac	= ROOT.TH1F("sigfrac_"+str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac),"sigfrac_"+str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac),100000,-5000,5000)
        h_theta1	= ROOT.TH1F("theta1_" +str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac),"theta1_" +str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac),2000,-100,100)
        h_theta2	= ROOT.TH1F("theta2_" +str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac),"theta2_" +str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac),2000,-100,100)
        h_theta3	= ROOT.TH1F("theta3_" +str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac),"theta3_" +str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac),2000,-100,100)
        h_theta4	= ROOT.TH1F("theta4_" +str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac),"theta4_" +str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac),2000,-100,100)
        for N in range(niter):
          if N==0: continue
#          print(N)
          root_i=input_folder+((((root.replace('XINTP',str(prec))).replace('XNBEV',str(N_ev))).replace('XNBINS',str(N_bins))).replace('XSFRAC',str(Sig_frac))).replace('XITN',str(N))
#          print('ROOT file name:\t'+str(root_i))
          if not os.path.isfile(root_i) :
            print('WARNING: '+root_i+' does not exist')
            continue
          f_i 	= ROOT.TFile(root_i,'READ')
          rfr_i	= f_i.Get('fitResult')
#          rfr_i.Print()
          pars = rfr_i.floatParsFinal()
          iter = pars.createIterator()
          var = iter.Next()
          while var :
            name_par = var.GetName()
#            print name_par
            # fill histos with values
            if name_par=='sigfrac':	h_sigfrac.Fill((var.getVal()-float(Sig_frac))/var.getError())
            if name_par=='theta1': 	h_theta1.Fill( (var.getVal()-(-0.7) )/var.getError())
            if name_par=='theta2': 	h_theta2.Fill( (var.getVal()-(-0.05))/var.getError())
            if name_par=='theta3': 	h_theta3.Fill( (var.getVal()-(-0.1) )/var.getError())
            if name_par=='theta4': 	h_theta4.Fill( (var.getVal()-(0.05) )/var.getError())

            # next floating par
            var = iter.Next()
          f_i.Close()
        f_out.cd()
        h_sigfrac.Write() 
        h_theta1 .Write()  
        h_theta2 .Write()  
        h_theta3 .Write()  
        h_theta4 .Write()  
  
#f_out.Write()
f_out.Close()

print('Done.')
