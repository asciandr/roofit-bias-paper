#!/bin/bash

setupATLAS

# Setup software
lsetup "views LCG_97_ATLAS_1 x86_64-centos7-gcc8-opt"

# Setup pre-view of v6.24 RooFit
source /afs/cern.ch/user/a/asciandr/work/public/ROOT_build_RooBinSamplingPdf/build/bin/thisroot.sh
