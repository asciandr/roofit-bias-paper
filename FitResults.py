#!/usr/bin/env python

import os.path
from array import array

import ROOT
from ROOT import gStyle,gPad

# switches
withIntegration = True#False#True
scanNev = True#False#True

# invariant names
in_root	= 'Nbins_out_plots.root'
if scanNev:
  in_root = 'Nev_out_plots.root'
out_root= 'fit_plots.root'

# variable settings
nev={500000}
if scanNev:
  nev={500,5000,50000,500000,50000000}
nbins={"8","16","32","128","256","512"}
if scanNev:
  nbins={"32"}
sigfrac={"0.015000"}
int_prec={"0.000000"}
if withIntegration:
  int_prec={"0.001000"}

# fit options
f_gaus = ROOT.TF1("f_gaus","gaus",-2000,2000)
rebin=4

def beautify_graph(graph, xaxistitle, title):
  # output 
  out_folder = 'paper_plots/'
  if scanNev:
    out_folder = out_folder+'Nev_'
  if withIntegration:
    out_folder = 'wInt_'+out_folder
  else:
    out_folder = 'woInt_'+out_folder

  graph.SetTitle(title)
  graph.SetLineWidth(1)
  graph.SetLineColor(ROOT.kBlue)
  graph.SetMarkerStyle(8)
  graph.SetMarkerSize(2)
  graph.SetMarkerColorAlpha(ROOT.kBlue,0.)
  graph.GetXaxis().SetTitle(xaxistitle)
  graph.GetXaxis().CenterTitle(True)
  graph.GetXaxis().SetTitleSize(0.07)
  graph.GetXaxis().SetLabelSize(0.06)
  graph.GetXaxis().SetTitleOffset(1.2)
  graph.GetXaxis().SetMaxDigits(3)
  graph.GetXaxis().SetNdivisions(6, 5, 0)
  graph.GetYaxis().SetTitle('Pull #mu #pm #sigma')
  graph.GetYaxis().CenterTitle(True)
  graph.GetYaxis().SetTitleSize(0.07)
  graph.GetYaxis().SetLabelSize(0.06)
  graph.GetYaxis().SetTitleOffset(1)
  graph.GetYaxis().SetNdivisions(4, 5, 0)

  gStyle.SetTitleFontSize(0.065)

  c = ROOT.TCanvas(graph.GetName(),graph.GetName(),800,600)
  if scanNev:
    c.SetLogx()
  c.SetLeftMargin(0.15)
  c.SetRightMargin(0.15)
  c.SetBottomMargin(0.18)
  c.SetTopMargin(0.10)
  graph.Draw('ap')

  # add horizontal TLine at 0
  x1=graph.GetXaxis().GetBinLowEdge(1)
  x2=graph.GetXaxis().GetBinUpEdge(graph.GetXaxis().GetNbins())
  line = ROOT.TLine(x1,0,x2,0)
  line.SetLineColor(ROOT.kRed)
  line.SetLineWidth(1)
  line.Draw()
  gPad.Update()

  c.SaveAs(out_folder+graph.GetName()+'.pdf')
  c.SaveAs(out_folder+graph.GetName()+'.eps')
  c.SaveAs(out_folder+graph.GetName()+'.C')
  
#def beautify_graph(graph):

# main
def main():
   
  # output histograms
  mean_nbins      	= array( 'd' ) 
  zero      		= array( 'd' ) 
  mean_sigfrac       	= array( 'd' ) 
  mean_theta1        	= array( 'd' ) 
  mean_theta2        	= array( 'd' ) 
  mean_theta3        	= array( 'd' ) 
  mean_theta4        	= array( 'd' ) 
  err_mean_nbins      	= array( 'd' ) 
  err_mean_sigfrac      = array( 'd' ) 
  err_mean_theta1       = array( 'd' ) 
  err_mean_theta2       = array( 'd' ) 
  err_mean_theta3       = array( 'd' ) 
  err_mean_theta4       = array( 'd' ) 
   
  f_in		= ROOT.TFile(in_root,'READ')
  f_out           = ROOT.TFile(out_root,'RECREATE')
  for nprec,prec in enumerate(int_prec):
  #  print(prec)
    for N_ev in nev:
  #    print(N_ev)
      for N_bins in nbins:
  #      print(N_bins)
        for Sig_frac in sigfrac:
  #        print(Sig_frac)
          if scanNev:
            mean_nbins.append(float(N_ev))
          else:
            mean_nbins.append(float(N_bins))
          zero.append(0)
      
          # input histograms
          h_sigfrac	= f_in.Get("sigfrac_"+str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac))
          h_sigfrac.Rebin(rebin)
          h_sigfrac.Fit(f_gaus)
          mean_sigfrac.append(f_gaus.GetParameter(1))
          err_mean_sigfrac.append(f_gaus.GetParameter(2))
  
          h_theta1	= f_in.Get("theta1_" +str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac))
          h_theta1.Rebin(rebin)
          h_theta1.Fit(f_gaus)
          mean_theta1.append(f_gaus.GetParameter(1))
          err_mean_theta1.append(f_gaus.GetParameter(2))
  
          h_theta2	= f_in.Get("theta2_" +str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac))
          h_theta2.Rebin(rebin)
          h_theta2.Fit(f_gaus)
          mean_theta2.append(f_gaus.GetParameter(1))
          err_mean_theta2.append(f_gaus.GetParameter(2))
  
          h_theta3	= f_in.Get("theta3_" +str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac))
          h_theta3.Rebin(rebin)
          h_theta3.Fit(f_gaus)
          mean_theta3.append(f_gaus.GetParameter(1))
          err_mean_theta3.append(f_gaus.GetParameter(2))
  
          h_theta4	= f_in.Get("theta4_" +str(prec)+"_"+str(N_ev)+"_"+str(N_bins)+"_"+str(Sig_frac))
          h_theta4.Rebin(rebin)
          h_theta4.Fit(f_gaus)
          mean_theta4.append(f_gaus.GetParameter(1))
          err_mean_theta4.append(f_gaus.GetParameter(2))
  
  
  f_out.cd()
  
  print(type(len(mean_nbins)))
  print(type(mean_nbins))
  print(type(mean_sigfrac))
  
  N = len(mean_nbins)
  h_mean_sigfrac	= ROOT.TGraphErrors(N,	mean_nbins,	mean_sigfrac,	zero,	err_mean_sigfrac)
  h_mean_sigfrac.SetName('mean_sigfrac')
  h_mean_theta1 = ROOT.TGraphErrors(N,  mean_nbins,     mean_theta1,   zero,   err_mean_theta1)
  h_mean_theta1.SetName('mean_theta1')
  h_mean_theta2 = ROOT.TGraphErrors(N,  mean_nbins,     mean_theta2,   zero,   err_mean_theta2)
  h_mean_theta2.SetName('mean_theta2')
  h_mean_theta3 = ROOT.TGraphErrors(N,  mean_nbins,     mean_theta3,   zero,   err_mean_theta3)
  h_mean_theta3.SetName('mean_theta3')
  h_mean_theta4 = ROOT.TGraphErrors(N,  mean_nbins,     mean_theta4,   zero,   err_mean_theta4)
  h_mean_theta4.SetName('mean_theta4')
   
  xAxisTitle='N_{bins}'
  if scanNev:
    xAxisTitle='N_{ev}'
  beautify_graph(h_mean_sigfrac,	xAxisTitle,	'f_{sig}')
  beautify_graph(h_mean_theta1 ,	xAxisTitle,	'#theta_{1}')
  beautify_graph(h_mean_theta2 ,	xAxisTitle,	'#theta_{2}')
  beautify_graph(h_mean_theta3 ,	xAxisTitle,	'#theta_{3}')
  beautify_graph(h_mean_theta4 ,	xAxisTitle,	'#theta_{4}')

  h_mean_sigfrac.Write()
  h_mean_theta1 .Write()
  h_mean_theta2 .Write()
  h_mean_theta3 .Write()
  h_mean_theta4 .Write()
  
  f_out.Close()
  f_in.Close()
  
  print('Done.')

if __name__ == '__main__':
  main()

